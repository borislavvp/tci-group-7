package casino.gamingmachine;

import casino.bet.Bet;
import casino.bet.BetResult;
import casino.bet.MoneyAmount;
import casino.cashier.BetNotExceptedException;
import casino.cashier.ICashier;
import casino.cashier.IGamblerCard;
import casino.cashier.InvalidAmountException;
import casino.game.IGame;
import casino.game.NoCurrentRoundException;
import casino.idfactory.BetID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GamingMachineTest {
    private static final ICashier _mockCashier = mock(ICashier.class);

    private static final IGame _mockGame = mock(IGame.class);

    private static final IGamblerCard _mockCard = mock(IGamblerCard.class);

    private static final MoneyAmount _mockMoneyAmount = mock(MoneyAmount.class);

    private static final BetID BET_ID = mock(BetID.class);
    private static final BetID ANOTHER_BET_ID = mock(BetID.class);

    private static final Bet _mockBet = mock(Bet.class);
    private static final BetResult _mockBetResult = mock(BetResult.class);

    private static final long BET_AMOUNT = 11;

    /**
     * Clear all of the mocks after each test runs
     */
    @AfterEach
    void MockitoResetMocks(){
        Mockito.reset(_mockCard,_mockCashier,_mockGame,_mockMoneyAmount,_mockBet,_mockBetResult);
    }

    /**
     * @verifies ShouldSetTheMachineCardToThePassedGamblerCard
     * @see GamingMachine#connectCard(IGamblerCard)
     */
    @Test
    void connectCard_ShouldSetTheMachineCardToThePassedGamblerCard() {
        //Arrange
        GamingMachine machine = new GamingMachine(_mockCashier,_mockGame);

        //Act
        machine.connectCard(_mockCard);

        //Assert
        assertEquals(machine.GetConnectedCard(),_mockCard);
    }

    /**
     * @verifies ShouldRemoveTheMachineCardWhenNoBetIsOpen
     * @see GamingMachine#disconnectCard()
     */
    @Test
    void disconnectCard_ShouldRemoveTheMachineCardWhenNoBetIsOpen() throws CurrentBetMadeException {
        //Arrange
        GamingMachine machine = new GamingMachine(_mockCashier,_mockGame);

        machine.connectCard(_mockCard);

        //Act
        machine.disconnectCard();

        //Assert
        assertNull(machine.GetConnectedCard());
    }

    /**
     * @verifies ShouldThrowCurrentBetMadeExceptionWhenBetIsOpen
     * @see GamingMachine#placeBet(long)
     */
    @Test
    void disconnectCard_ShouldThrowCurrentBetMadeExceptionWhenBetIsOpen() throws NoPlayerCardException, BetNotExceptedException {
        //Arrange
        GamingMachine machine = new GamingMachine(_mockCashier,_mockGame);

        when(_mockCard.generateNewBetID()).thenReturn(BET_ID);
        when(_mockCashier.checkIfBetIsValid(any(IGamblerCard.class),any(Bet.class))).thenReturn(true);

        //Act
        machine.connectCard(_mockCard);
        boolean IsBetMade = machine.placeBet(BET_AMOUNT);

        //Assert
        assertThrows(CurrentBetMadeException.class, machine::disconnectCard);
    }

    /**
     * @verifies ShouldSubmitBetWhenTheGivenAmountIsValidAndCardIsConnected
     * @see GamingMachine#placeBet(long)
     */
    @Test
    void placeBet_ShouldSubmitSuccessfullyBetWhenTheGivenAmountIsValidAndCardIsConnected() throws NoPlayerCardException, BetNotExceptedException, NoCurrentRoundException {
        //Arrange
        GamingMachine machine = new GamingMachine(_mockCashier,_mockGame);

        when(_mockCard.generateNewBetID()).thenReturn(BET_ID);
        when(_mockCashier.checkIfBetIsValid(any(IGamblerCard.class),any(Bet.class))).thenReturn(true);
        when(_mockGame.acceptBet(any(Bet.class),any(IGamingMachine.class))).thenReturn(true);

        machine.connectCard(_mockCard);
        //Act
        boolean IsBetMade = machine.placeBet(BET_AMOUNT);

        //Assert
        verify(_mockGame).acceptBet(any(Bet.class),any(IGamingMachine.class));
        assertTrue(IsBetMade);
    }

    /**
     * @verifies ShouldNotSubmitBetWhenTheGivenAmountIsInValid
     * @see GamingMachine#placeBet(long)
     */
    @Test
    void placeBet_ShouldNotSubmitBetWhenTheGivenAmountIsInValid() throws NoPlayerCardException, BetNotExceptedException {
        //Arrange
        GamingMachine machine = new GamingMachine(_mockCashier,_mockGame);

        when(_mockCard.generateNewBetID()).thenReturn(BET_ID);
        when(_mockCashier.checkIfBetIsValid(any(IGamblerCard.class),any(Bet.class))).thenThrow(BetNotExceptedException.class);

        machine.connectCard(_mockCard);
        //Act
        boolean IsBetMade = machine.placeBet(BET_AMOUNT);

        //Assert
        assertFalse(IsBetMade);
        verifyNoInteractions(_mockGame);
    }

    /**
     * @verifies ShouldNotSubmitBetAndThrowNoPlayerCardExceptionWhenNoCardIsConnected
     * @see GamingMachine#placeBet(long)
     */
    @Test
    void placeBet_ShouldNotSubmitBetAndThrowNoPlayerCardExceptionWhenNoCardIsConnected(){
        //Arrange
        GamingMachine machine = new GamingMachine(_mockCashier,_mockGame);

        //Assert
        assertThrows(NoPlayerCardException.class, () -> machine.placeBet(BET_AMOUNT));
    }

    /**
     * @verifies ShouldIncreaseTheBalanceWhenTheWinnerMadeBetOnThisMachine
     * @see GamingMachine#acceptWinner(BetResult)
     */
    @Test
    void acceptWinner_ShouldIncreaseTheBalanceWhenTheWinnerMadeBetOnThisMachine() throws InvalidAmountException, NoPlayerCardException {

        //Arrange
        GamingMachine machine = new GamingMachine(_mockCashier,_mockGame);

        when(_mockCard.generateNewBetID()).thenReturn(BET_ID);
        when(_mockBet.getBetID()).thenReturn(BET_ID);
        when(_mockBetResult.getWinningBet()).thenReturn(_mockBet);
        when(_mockBetResult.getAmountWon()).thenReturn(_mockMoneyAmount);

        machine.connectCard(_mockCard);
        machine.placeBet(BET_AMOUNT);
        //Act
        machine.acceptWinner(_mockBetResult);

        //Assert
        verify(_mockCashier).addAmount(_mockCard,_mockMoneyAmount);
    }

    /**
     * @verifies ShouldNotIncreaseTheBalanceWhenTheWinnerMadeBetOnThisMachine
     * @see GamingMachine#acceptWinner(BetResult)
     */
    @Test
    void acceptWinner_ShouldNotIncreaseTheBalanceWhenTheWinnerDidNotMadeBetOnThisMachine() throws NoPlayerCardException, InvalidAmountException {

        //Arrange
        GamingMachine machine = new GamingMachine(_mockCashier,_mockGame);

        when(_mockCard.generateNewBetID()).thenReturn(BET_ID);
        when(_mockBet.getBetID()).thenReturn(ANOTHER_BET_ID);
        when(_mockBetResult.getWinningBet()).thenReturn(_mockBet);
        when(_mockBetResult.getAmountWon()).thenReturn(_mockMoneyAmount);

        machine.connectCard(_mockCard);
        machine.placeBet(BET_AMOUNT);

        //Act
        machine.acceptWinner(_mockBetResult);

        //Assert
        verify(_mockCashier,never()).addAmount(any(IGamblerCard.class),any(MoneyAmount.class));
    }

    /**
     * @verifies ShouldNotIncreaseTheBalanceWhenTheWinnerMadeBetOnThisMachine
     * @see GamingMachine#acceptWinner(BetResult)
     */
    @Test
    void acceptWinner_ShouldNotClearTheOpenBetWhenAfterAcceptingTheWinner() throws NoPlayerCardException {

        //Arrange
        GamingMachine machine = new GamingMachine(_mockCashier,_mockGame);

        when(_mockCard.generateNewBetID()).thenReturn(BET_ID);
        when(_mockBet.getBetID()).thenReturn(ANOTHER_BET_ID);
        when(_mockBetResult.getWinningBet()).thenReturn(_mockBet);
        when(_mockBetResult.getAmountWon()).thenReturn(_mockMoneyAmount);

        machine.connectCard(_mockCard);
        machine.placeBet(BET_AMOUNT);

        //Act
        machine.acceptWinner(_mockBetResult);

        //Assert
        assertNull(machine.GetOpenBet());
    }
    /**
     * @verifies ShouldNotIncreaseTheBalanceWhenTheWinnerMadeBetOnThisMachine
     * @see GamingMachine#acceptWinner(BetResult)
     */
    @Test
    void acceptWinner_ShouldThrowIllegalArgumentExceptionWhenCashierThrowsInvalidAmountException() throws NoPlayerCardException, InvalidAmountException {

        //Arrange
        GamingMachine machine = new GamingMachine(_mockCashier,_mockGame);

        when(_mockCard.generateNewBetID()).thenReturn(BET_ID);
        when(_mockBet.getBetID()).thenReturn(BET_ID);
        when(_mockBetResult.getWinningBet()).thenReturn(_mockBet);
        when(_mockBetResult.getAmountWon()).thenReturn(_mockMoneyAmount);
        doThrow(InvalidAmountException.class).when(_mockCashier).addAmount(any(IGamblerCard.class),any(MoneyAmount.class));

        machine.connectCard(_mockCard);
        machine.placeBet(BET_AMOUNT);

        //Act
        assertThrows(IllegalArgumentException.class,() -> machine.acceptWinner(_mockBetResult));

    }

}