package casino.cashier;

import casino.idfactory.BetID;
import casino.idfactory.CardID;
import casino.idfactory.IDFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;



/**
 * Created by Redzhep
 */
public class GamblerCardTest {


    /**
     * @verifies return all generated betID's by the card
     * @see GamblerCard#returnBetIDs()
     */
    @Test
    public void returnBetIDs_shouldReturnAllGeneratedBetIDsByTheCard() throws Exception {

        //arrange
        GamblerCard SUT = new GamblerCard(); // SUT
        Set<BetID> betIDS = new HashSet<BetID>();;
        //act
        betIDS.add(SUT.generateNewBetID());
        betIDS.add(SUT.generateNewBetID());
        betIDS.add(SUT.generateNewBetID());
        //assert
        assertEquals(betIDS, SUT.returnBetIDs());
    }

    /**
     * @verifies return all generated betID's by the card
     * @see GamblerCard#returnBetIDsAndClearCard()
     */
    @Test
    public void returnBetIDsAndClearCard_shouldReturnAllGeneratedBetIDsByTheCard() throws Exception {
        //arrange
        GamblerCard SUT = new GamblerCard(); // SUT
        Set<BetID> betIDS = new HashSet<BetID>();
        //act
        betIDS.add(SUT.generateNewBetID());
        betIDS.add(SUT.generateNewBetID());
        betIDS.add(SUT.generateNewBetID());
        //assert
        assertEquals(betIDS, SUT.returnBetIDsAndClearCard());
    }

    /**
     * @verifies return all generated betID's and remove them fom the card
     * @see GamblerCard#returnBetIDsAndClearCard()
     */
    @Test
    public void returnBetIDsAndClearCard_shouldReturnAllGeneratedBetIDsAndRemoveThemFomTheCard() throws Exception {

        //arrange
        GamblerCard SUT = new GamblerCard(); // SUT
        //act
        SUT.generateNewBetID();
        SUT.generateNewBetID();
        SUT.generateNewBetID();
        SUT.returnBetIDsAndClearCard();
        //assert
        assertEquals(0, SUT.getNumberOfBetIDs());
        assertTrue(SUT.returnBetIDs().isEmpty());

    }

    /**
     * @verifies generates a unique betID
     * @see GamblerCard#generateNewBetID()
     */
    @Test
    public void generateNewBetID_shouldGeneratesAUniqueBetID() throws Exception {

        //arrange
        GamblerCard SUT = new GamblerCard(); // SUT
        //act
        BetID betIDOne = SUT.generateNewBetID();
        BetID betIDTwo = SUT.generateNewBetID();
        //assert
        assertNotNull(betIDOne);
        assertNotNull(betIDTwo);
        assertNotEquals(betIDTwo, betIDOne);

    }

    /**
     * @verifies store the generated betID in the card
     * @see GamblerCard#generateNewBetID()
     */
    @Test
    public void generateNewBetID_shouldStoreTheGeneratedBetIDInTheCard() throws Exception {

        //arrange
        GamblerCard SUT = new GamblerCard(); // SUT
        Set<BetID> betIDS = new HashSet<BetID>();
        //act
        betIDS.add(SUT.generateNewBetID());
        betIDS.add(SUT.generateNewBetID());
        betIDS.add(SUT.generateNewBetID());
        //assert
        assertEquals(betIDS, SUT.returnBetIDs());
    }

    /**
     * @verifies return number of betID's generated on this card.
     * @see GamblerCard#getNumberOfBetIDs()
     */
    @Test
    public void getNumberOfBetIDs_shouldReturnNumberOfBetIDsGeneratedOnThisCard() throws Exception {

        //arrange
        GamblerCard SUT = new GamblerCard();
        //act
        SUT.generateNewBetID();
        SUT.generateNewBetID();
        SUT.generateNewBetID();
        //assert
        assertEquals(3, SUT.getNumberOfBetIDs());
    }

    /**
     * @verifies return the cardID
     * @see GamblerCard#getCardID()
     */
    @Test
    public void getCardID_shouldReturnTheCardID() throws Exception {
        //arrange  //act
        GamblerCard SUT = new GamblerCard();
        //assert
        assertThat(SUT.getCardID()).isInstanceOf(CardID.class);
    }
}
