package casino.cashier;

import casino.bet.Bet;
import casino.bet.MoneyAmount;
import casino.idfactory.BetID;
import casino.idfactory.CardID;
import casino.idfactory.GeneralID;
import casino.idfactory.IDFactory;
import gamblingauthoritiy.BetLoggingAuthority;
import gamblingauthoritiy.IBetLoggingAuthority;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class CashierTest {

    /**
     * @verifies callBetLoggingAuthorityToLogHandOutGamblingCard
     * @see Cashier#distributeGamblerCard()
     */
    @Test
    public void distributeGamblerCard_shouldCallBetLoggingAuthorityToLogHandOutGamblingCard() throws Exception {
        //arrange

        //arrange DOCs
        BetLoggingAuthority loggingAuthority = mock(BetLoggingAuthority.class);

        //arrange SUT
        Cashier SUT = new Cashier(loggingAuthority);

        //act
        GamblerCard gamblerCard = (GamblerCard) SUT.distributeGamblerCard();

        //assert
        verify(loggingAuthority, description("Bet logging authority is not called logHandOutGamblingCard!")).logHandOutGamblingCard(gamblerCard.getCardID());
    }

    /**
     * @verifies addNewGamblerCardToTheGamblerCardsList
     * @see Cashier#distributeGamblerCard()
     */
    @Test
    public void distributeGamblerCard_shouldAddNewGamblerCardToTheGamblerCardsList() throws Exception {
        //arrange

        //arrange DOCs
        BetLoggingAuthority loggingAuthority = mock(BetLoggingAuthority.class);

        //arrange SUT
        Cashier SUT = new Cashier(loggingAuthority);

        //act
        SUT.distributeGamblerCard();
        SUT.distributeGamblerCard();
        SUT.distributeGamblerCard();

        //assert
        assertNotNull(SUT.getGamblerCardList());
        assertEquals(3, SUT.getGamblerCardList().size());
    }

    /**
     * @verifies callReturnBetIDsAndClearCardFromGamblerCard
     * @see Cashier#returnGamblerCard(IGamblerCard)
     */
    @Test
    public void returnGamblerCard_shouldCallReturnBetIDsAndClearCardFromGamblerCard() throws Exception {
        //arrange
        Set<BetID> betIDS = new HashSet<>();
        //arrange DOCs
        BetLoggingAuthority loggingAuthority = mock(BetLoggingAuthority.class);

        //arrange SUT
        Cashier SUT = new Cashier(loggingAuthority);

        //act
        GamblerCard gc = (GamblerCard) SUT.distributeGamblerCard();
        SUT.returnGamblerCard(gc);

        //assert
        assertThat(gc.returnBetIDs()).isEqualTo(betIDS);
    }

    /**
     * @verifies callBetLoggingAuthorityToLogHandInGamblingCard
     * @see Cashier#returnGamblerCard(IGamblerCard)
     */
    @Test
    public void returnGamblerCard_shouldCallBetLoggingAuthorityToLogHandInGamblingCard() throws Exception {
        //arrange
        //arrange DOCs
        BetLoggingAuthority loggingAuthority = mock(BetLoggingAuthority.class);

        //arrange SUT
        Cashier SUT = new Cashier(loggingAuthority);

        //act
        GamblerCard gc = (GamblerCard) SUT.distributeGamblerCard();
        SUT.returnGamblerCard(gc);

        //assert
        verify(loggingAuthority, description("Bet logging authority is not called logHandInGamblingCard!")).logHandInGamblingCard(gc.getCardID(), gc.returnBetIDs());
    }

    /**
     * @verifies removeTheGamblerCardFromTheGamblerCardsList
     * @see Cashier#returnGamblerCard(IGamblerCard)
     */
    @Test
    public void returnGamblerCard_shouldRemoveTheGamblerCardFromTheGamblerCardsList() throws Exception {
        //arrange
        //arrange DOCs
        BetLoggingAuthority loggingAuthority = mock(BetLoggingAuthority.class);

        //arrange SUT
        Cashier SUT = new Cashier(loggingAuthority);

        //act
        GamblerCard gamblerCard1 = (GamblerCard) SUT.distributeGamblerCard();
        GamblerCard gamblerCard2 = (GamblerCard) SUT.distributeGamblerCard();
        GamblerCard gamblerCard3 = (GamblerCard) SUT.distributeGamblerCard();
        SUT.returnGamblerCard(gamblerCard1);
        SUT.returnGamblerCard(gamblerCard2);
        SUT.returnGamblerCard(gamblerCard3);

        //assert
        assertThat(SUT.getGamblerCardList().size()).isEqualTo(0);
    }

    /**
     * @verifies removeTheGamblerCardFromTheHashMap
     * @see Cashier#returnGamblerCard(IGamblerCard)
     */
    @Test
    public void returnGamblerCard_shouldRemoveTheGamblerCardFromTheHashMap() throws Exception {
        //arrange

        //arrange DOCs
        BetLoggingAuthority loggingAuthority = mock(BetLoggingAuthority.class);

        //arrange SUT
        Cashier SUT = new Cashier(loggingAuthority);

        //act
        GamblerCard gamblerCard1 = (GamblerCard) SUT.distributeGamblerCard();
        GamblerCard gamblerCard2 = (GamblerCard) SUT.distributeGamblerCard();
        GamblerCard gamblerCard3 = (GamblerCard) SUT.distributeGamblerCard();
        SUT.returnGamblerCard(gamblerCard1);
        SUT.returnGamblerCard(gamblerCard2);
        SUT.returnGamblerCard(gamblerCard3);

        //assert
        assertThat(SUT.getMoneyAmountHashMap().size()).isEqualTo(0);
    }

    /**
     * @verifies acceptTheBetIfTheAmountOnTheCardIsEnough
     * @see Cashier#checkIfBetIsValid(IGamblerCard, Bet)
     */
    @Test
    public void checkIfBetIsValid_shouldAcceptTheBetIfTheAmountOnTheCardIsEnough() throws Exception {
        //arrange
        Long VALID_BET_AMOUNT = 5L;
        Long VALID_CARD_AMOUNT = 15L;
        //arrange DOCs
        BetLoggingAuthority loggingAuthority = mock(BetLoggingAuthority.class);

        MoneyAmount amountCard = mock(MoneyAmount.class);
        when(amountCard.getAmountInCents()).thenReturn(VALID_CARD_AMOUNT);

        MoneyAmount amountBet = mock(MoneyAmount.class);
        when(amountBet.getAmountInCents()).thenReturn(VALID_BET_AMOUNT);

        Bet betToDo  = mock(Bet.class);
        when(betToDo.getMoneyAmount()).thenReturn(amountBet);

        //arrange SUT
        Cashier SUT = new Cashier(loggingAuthority);
        GamblerCard card = (GamblerCard) SUT.distributeGamblerCard();
        SUT.addAmount(card, amountCard);

        //act
        boolean bool = SUT.checkIfBetIsValid(card, betToDo);

        //assert
        assertThat(bool).isEqualTo(true);
    }

    /**
     * @verifies withdrawnTheBetAmountFromTheCard
     * @see Cashier#checkIfBetIsValid(IGamblerCard, Bet)
     */
    @Test
    public void checkIfBetIsValid_shouldWithdrawnTheBetAmountFromTheCard() throws Exception {
        //arrange
        Long VALID_BET_AMOUNT = 5L;
        Long VALID_CARD_AMOUNT = 15L;
        Long LEFT_AMOUNT = VALID_CARD_AMOUNT - VALID_BET_AMOUNT;

        //arrange DOCs
        BetLoggingAuthority loggingAuthority = mock(BetLoggingAuthority.class);

        MoneyAmount amountCard = mock(MoneyAmount.class);
        when(amountCard.getAmountInCents()).thenReturn(VALID_CARD_AMOUNT);

        MoneyAmount amountBet = mock(MoneyAmount.class);
        when(amountBet.getAmountInCents()).thenReturn(VALID_BET_AMOUNT);

        Bet betToDo  = mock(Bet.class);
        when(betToDo.getMoneyAmount()).thenReturn(amountBet);

        //arrange SUT
        Cashier SUT = new Cashier(loggingAuthority);
        GamblerCard card = (GamblerCard) SUT.distributeGamblerCard();
        SUT.addAmount(card, amountCard);

        //act
        SUT.checkIfBetIsValid(card, betToDo);

        //assert
        MoneyAmount leftAmount = SUT.getMoneyAmountHashMap().get(card.getCardID());
        assertThat(leftAmount.getAmountInCents()).isEqualTo(LEFT_AMOUNT);
    }
    /**
     * @verifies throwBetNotValidExceptionIfBetAmountIsInvalid
     * @see Cashier#checkIfBetIsValid(IGamblerCard, Bet)
     */
    @Test
    public void checkIfBetIsValid_shouldThrowBetNotValidExceptionIfBetAmountIsInvalid() throws Exception {
        //arrange
        Long VALID_BET_AMOUNT = 15L;
        Long VALID_CARD_AMOUNT = 5L;

        //arrange DOCs
        BetLoggingAuthority loggingAuthority = mock(BetLoggingAuthority.class);

        MoneyAmount amountCard = mock(MoneyAmount.class);
        when(amountCard.getAmountInCents()).thenReturn(VALID_CARD_AMOUNT);

        MoneyAmount amountBet = mock(MoneyAmount.class);
        when(amountBet.getAmountInCents()).thenReturn(VALID_BET_AMOUNT);

        Bet betToDo  = mock(Bet.class);
        when(betToDo.getMoneyAmount()).thenReturn(amountBet);

        //arrange SUT
        Cashier SUT = new Cashier(loggingAuthority);
        GamblerCard card = (GamblerCard) SUT.distributeGamblerCard();
        SUT.addAmount(card, amountCard);

        //act & assert
        assertThrows(BetNotExceptedException.class, () -> SUT.checkIfBetIsValid(card, betToDo), "Should throw BetNotValidException");
    }

    /**
     * @verifies updateMoneyOnTheGamblerCardByHashMap
     * @see Cashier#addAmount(IGamblerCard, MoneyAmount)
     */
    @Test
    public void addAmount_shouldUpdateMoneyOnTheGamblerCardByHashMap() throws Exception {
        //arrange
        Long VALID_AMOUNT = 15L;

        //arrange DOCs
        BetLoggingAuthority loggingAuthority = mock(BetLoggingAuthority.class);

        MoneyAmount amountToAdd = mock(MoneyAmount.class);
        when(amountToAdd.getAmountInCents()).thenReturn(VALID_AMOUNT);

        //arrange SUT
        Cashier SUT = new Cashier(loggingAuthority);
        GamblerCard card = (GamblerCard) SUT.distributeGamblerCard();

        //act
        SUT.addAmount(card, amountToAdd);

        //assert
        MoneyAmount cardAmount = SUT.getMoneyAmountHashMap().get(card.getCardID());
        assertThat(cardAmount).isEqualTo(amountToAdd);
    }

    /**
     * @verifies throwInvalidAmountExceptionWhenMoneyAmountIsNegative
     * @see Cashier#addAmount(IGamblerCard, MoneyAmount)
     */
    @Test
    public void addAmount_shouldThrowInvalidAmountExceptionWhenMoneyAmountIsNegative() throws Exception {
        //arrange
        Long INVALID_AMOUNT = -15L;

        //arrange DOCs
        BetLoggingAuthority loggingAuthority = mock(BetLoggingAuthority.class);

        MoneyAmount amountToAdd = mock(MoneyAmount.class);
        when(amountToAdd.getAmountInCents()).thenReturn(INVALID_AMOUNT);

        //arrange SUT
        Cashier SUT = new Cashier(loggingAuthority);
        GamblerCard card = (GamblerCard) SUT.distributeGamblerCard();

        //act & assert
        assertThrows(InvalidAmountException.class, () -> SUT.addAmount(card, amountToAdd), "Should throw InvalidAmountException");
    }

}
