package casino.game;

import casino.bet.Bet;
import casino.bet.BetResult;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class GameRuleTest {

    private static final int MAXIMUM_BETS = 11;
    private static final int WINNER_VALUE = new Random().nextInt();
    private static final Set<Bet> VALID_BETS_SET = new HashSet<>(
            Arrays.asList(
                    mock(Bet.class),
                    mock(Bet.class),
                    mock(Bet.class),
                    mock(Bet.class)
            )
    );

    private static final Set<Bet> INVALID_EMPTY_BETS_SET = new HashSet<>();
    /**
     * @verifies determineWinner_ShouldReturnBetResultWhenValidWinnerValueAndCollectionOfBetsArePassed
     * @see GameRule#determineWinner(Integer, Set)
     */
    @Test
    void determineWinner_ShouldReturnBetResultWhenValidWinnerValueAndCollectionOfBetsArePassed() throws NoBetsMadeException {
        //Arrange
        GameRule rule = new GameRule(MAXIMUM_BETS);

        //Act
        BetResult result = rule.determineWinner(WINNER_VALUE,VALID_BETS_SET);

        //Assert
        assertNotNull(result);
    }
    /**
     * @verifies determineWinner_ShouldReturnBetResultWhichIsContainedInThePassedBetsWhenValidWinnerValueAndCollectionOfBetsArePassed
     * @see GameRule#determineWinner(Integer, Set)
     */
    @Test
    void determineWinner_ShouldReturnBetResultWhichIsContainedInThePassedBetsWhenValidWinnerValueAndCollectionOfBetsArePassed() throws NoBetsMadeException {
        //Arrange
        GameRule rule = new GameRule(MAXIMUM_BETS);

        //Act
        BetResult result = rule.determineWinner(WINNER_VALUE,VALID_BETS_SET);

        //Assert
        assertNotNull(result);
        assertTrue(VALID_BETS_SET.contains(result.getWinningBet()));
    }

    /**
     * @verifies ShouldThrowNoBetsMadeExceptionWhenInvalidCollectionOfBetsIsPassed
     * @see GameRule#determineWinner(Integer, Set)
     */
    @Test
    void determineWinner_ShouldThrowNoBetsMadeExceptionWhenInvalidCollectionOfBetsIsPassed() {
        //Arrange
        GameRule rule = new GameRule(MAXIMUM_BETS);

        //Assert
        assertThrows(NoBetsMadeException.class,() -> rule.determineWinner(WINNER_VALUE,INVALID_EMPTY_BETS_SET));
    }

}