package casino.game;

import casino.Casino;
import casino.bet.Bet;
import casino.bet.BetResult;
import casino.gamingmachine.IGamingMachine;
import casino.idfactory.BettingRoundID;
import gamblingauthoritiy.BetToken;
import gamblingauthoritiy.BettingAuthority;
import gamblingauthoritiy.IBetLoggingAuthority;
import gamblingauthoritiy.IBetTokenAuthority;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import static org.junit.jupiter.api.Assertions.*;

public class DefaultGameTest {


    @Rule
    public ExpectedException thrown = ExpectedException.none();

    IGameRule gameRule = mock(IGameRule.class);
    IBetTokenAuthority tokenAPI = mock(IBetTokenAuthority.class);
    IBetLoggingAuthority iBetLoggingAuthority = mock(IBetLoggingAuthority.class);
    IGamingMachine iGamingMachine = mock(IGamingMachine.class);



    /**
     * @verifies CreateAndStartNewBettingRound
     * @see DefaultGame#startBettingRound()
     */
    @Test
    public void startBettingRound_shouldCreateAndStartNewBettingRound() throws Exception {

        //arrange
        IGameRule gameRule = mock(IGameRule.class);
        IBetTokenAuthority tokenAPI = mock(IBetTokenAuthority.class);
        IBetLoggingAuthority iBetLoggingAuthority = mock(IBetLoggingAuthority.class);

        DefaultGame sut = new DefaultGame(gameRule, iBetLoggingAuthority, tokenAPI);

        //act
        BettingRound currentBettingRound = sut.getBettingRound();
        sut.startBettingRound();
        BettingRound newCurrentBettingRound = sut.getBettingRound();
        //assert
        assertNotEquals(currentBettingRound,newCurrentBettingRound);
    }

    /**
     * @verifies StopTheCurrentBettingRoundAndCreateANewOneWhichBecomesTheNewCurrentRound
     * @see DefaultGame#startBettingRound()
     */
    @Test
    public void startBettingRound_shouldStopTheCurrentBettingRoundAndCreateANewOneWhichBecomesTheNewCurrentRound() throws Exception {
        //arrange
        IGameRule gameRule = mock(IGameRule.class);
        IBetTokenAuthority tokenAPI = mock(IBetTokenAuthority.class);
        IBetLoggingAuthority iBetLoggingAuthority = mock(IBetLoggingAuthority.class);
        DefaultGame sut = new DefaultGame(gameRule, iBetLoggingAuthority, tokenAPI);

        //act
        sut.startBettingRound();
        BettingRound currentBettingRound = sut.getBettingRound();
        sut.startBettingRound();
        BettingRound newCurrentBettingRound = sut.getBettingRound();
        //assert
        assertNotEquals(currentBettingRound,newCurrentBettingRound);
    }

    /**
     * @verifies AcceptABetOnTheCurrentRound
     * @see DefaultGame#acceptBet(casino.bet.Bet, casino.gamingmachine.IGamingMachine)
     */
    @Test
    public void acceptBet_shouldAcceptABetOnTheCurrentRound() throws Exception {


    }

    /**
     * @verifies CheckIfARoundIsFinished
     * @see DefaultGame#acceptBet(casino.bet.Bet, casino.gamingmachine.IGamingMachine)
     */
    @Test
    public void acceptBet_shouldCheckIfARoundIsFinished() throws Exception {

        //arrange
        when(tokenAPI.getBetToken(any(BettingRoundID.class))).thenReturn(mock(BetToken.class));
        DefaultGame SUT = spy(new DefaultGame(gameRule,mock(IBetLoggingAuthority.class), tokenAPI));
        Bet bet = mock(Bet.class);
        //act
        SUT.setBettingRound(spy(SUT.getBettingRound()));
        SUT.acceptBet(bet,iGamingMachine);
        //assert
        verify(SUT).isBettingRoundFinished();


    }


    /**
     * @verifies DetermineTheWinnerWhenARoundIsFinished
     * @see DefaultGame#acceptBet(casino.bet.Bet, casino.gamingmachine.IGamingMachine)
     */
    @Test
    public void acceptBet_shouldDetermineTheWinnerWhenARoundIsFinished() throws Exception {

        //arrange
        when(tokenAPI.getBetToken(any(BettingRoundID.class))).thenReturn(mock(BetToken.class));
        DefaultGame SUT = spy(new DefaultGame(gameRule,mock(IBetLoggingAuthority.class), tokenAPI));
        Bet bet = mock(Bet.class);
        when(SUT.isBettingRoundFinished()).thenReturn(true);
        //act
        SUT.setBettingRound(spy(SUT.getBettingRound()));
        SUT.acceptBet(bet,iGamingMachine);
        //assert
        verify(SUT).determineWinner();
    }

    /**
     * @verifies EndTheCurrentBettingRound
     * @see DefaultGame#determineWinner()
     */
    @Test
    public void determineWinner_shouldEndTheCurrentBettingRound() throws Exception {

        //arrange
        when(tokenAPI.getBetToken(any(BettingRoundID.class))).thenReturn(mock(BetToken.class));
        DefaultGame SUT = spy(new DefaultGame(gameRule,mock(IBetLoggingAuthority.class), tokenAPI));
        Bet bet = mock(Bet.class);
        //act
        SUT.acceptBet(bet,iGamingMachine);
        SUT.determineWinner();
        //assert
        verify(SUT).startBettingRound();

    }

    /**
     * @verifies CalculateTheWinnerBasedOnTheGameRule
     * @see DefaultGame#determineWinner()
     */
    @Test
    public void determineWinner_shouldCalculateTheWinnerBasedOnTheGameRule() throws Exception {

        //arrange
        when(tokenAPI.getBetToken(any(BettingRoundID.class))).thenReturn(mock(BetToken.class));
        DefaultGame SUT = new DefaultGame(gameRule, iBetLoggingAuthority, tokenAPI);
        Bet bet = mock(Bet.class);
        //act
        SUT.acceptBet(bet,iGamingMachine);
        SUT.determineWinner();
        //assert
        verify(gameRule).determineWinner(anyInt(), anySet());

    }

    /**
     * @verifies NotifyAllMachinesAboutTheBetResult
     * @see DefaultGame#determineWinner()
     */
    @Test
    public void determineWinner_shouldNotifyAllMachinesAboutTheBetResult() throws Exception {
        //arrange
        when(tokenAPI.getBetToken(any(BettingRoundID.class))).thenReturn(mock(BetToken.class));
        DefaultGame SUT = new DefaultGame(gameRule, iBetLoggingAuthority, tokenAPI);
        BetResult betResult = mock(BetResult.class);
        Bet bet = mock(Bet.class);
        when(SUT.getGameRule().determineWinner(anyInt(),anySet())).thenReturn(betResult);
        //act
        SUT.acceptBet(bet,iGamingMachine);
        SUT.determineWinner();
        //assert
        verify(iGamingMachine).acceptWinner(betResult);

    }


    /**
     * @verifies EndTheCurrentRoundAndLogItToTheBetLoggingAuthority
     * @see DefaultGame#determineWinner()
     */
    @Test
    public void determineWinner_shouldEndTheCurrentRoundAndLogItToTheBetLoggingAuthority() throws Exception {

        //arrange
        when(tokenAPI.getBetToken(any(BettingRoundID.class))).thenReturn(mock(BetToken.class));
        DefaultGame SUT = new DefaultGame(gameRule, iBetLoggingAuthority, tokenAPI);
        BetResult betResult = mock(BetResult.class);
        Bet bet = mock(Bet.class);
        when(SUT.getGameRule().determineWinner(anyInt(),anySet())).thenReturn(betResult);
        //act
        SUT.acceptBet(bet,iGamingMachine);
        SUT.determineWinner();
        //assert
        verify(iBetLoggingAuthority).logEndBettingRound(any(BettingRound.class),any(BetResult.class));
    }

    /**
     * @verifies DetermineIfTheRightNrOfBetsAreDoneBasedOnGameRule
     * @see DefaultGame#isBettingRoundFinished()
     */
    @Test
    public void isBettingRoundFinished_shouldDetermineIfTheRightNrOfBetsAreDoneBasedOnGameRule() throws Exception {
        //arrange
        when(tokenAPI.getBetToken(any(BettingRoundID.class))).thenReturn(mock(BetToken.class));
        DefaultGame SUT = new DefaultGame(gameRule, iBetLoggingAuthority, tokenAPI);
        //act
        SUT.isBettingRoundFinished();
        //assert
        verify(gameRule).getMaxBetsPerRound();

    }

    /**
     * @verifies CallGetBettingRoundAndPlaceABet
     * @see DefaultGame#acceptBet(Bet, IGamingMachine)
     */
    @Test
    public void acceptBet_shouldCallGetBettingRoundAndPlaceABet() throws Exception {

        //arrange
        when(tokenAPI.getBetToken(any(BettingRoundID.class))).thenReturn(mock(BetToken.class));
        DefaultGame SUT = new DefaultGame(gameRule, iBetLoggingAuthority, tokenAPI);
        Bet bet = mock(Bet.class);
        //act
        SUT.setBettingRound(spy(SUT.getBettingRound()));
        SUT.acceptBet(bet,iGamingMachine);
        //assert
        verify(SUT.getBettingRound()).placeBet(bet);

    }

    /**
     * @verifies ThrowNewNoCurrentRoundExceptionWhenThereIsNoRound
     * @see DefaultGame#acceptBet(Bet, IGamingMachine)
     */

    @Test
    public void acceptBet_shouldThrowNewNoCurrentRoundExceptionWhenThereIsNoRound() throws Exception{

        //arrange
        when(tokenAPI.getBetToken(any(BettingRoundID.class))).thenReturn(mock(BetToken.class));
        DefaultGame SUT = spy(new DefaultGame(gameRule,mock(IBetLoggingAuthority.class), tokenAPI));
        Bet bet = mock(Bet.class);
        //act //assert
        SUT.setBettingRound(null);
        assertThrows(NoCurrentRoundException.class, () -> SUT.acceptBet(bet,iGamingMachine), "There is no betting round ");
    }

    /**
     * @verifies LogTheAcceptedBetToLoggerAuthority
     * @see DefaultGame#acceptBet(Bet, IGamingMachine)
     */
    @Test
    public void acceptBet_shouldLogTheAcceptedBetToLoggerAuthority() throws Exception {
        //arrange
        when(tokenAPI.getBetToken(any(BettingRoundID.class))).thenReturn(mock(BetToken.class));
        DefaultGame SUT = new DefaultGame(gameRule, iBetLoggingAuthority, tokenAPI);
        Bet bet = mock(Bet.class);
        //act
        SUT.setBettingRound(spy(SUT.getBettingRound()));
        SUT.acceptBet(bet,iGamingMachine);
        //assert
        verify(iBetLoggingAuthority).logAddAcceptedBet(bet,SUT.getBettingRound().getBettingRoundID(), iGamingMachine.getGamingMachineID());
    }
}
