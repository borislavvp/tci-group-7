package casino.game;

import casino.bet.Bet;
import casino.cashier.BetNotExceptedException;
import casino.gamingmachine.GamingMachine;
import casino.idfactory.BettingRoundID;
import casino.idfactory.GamingMachineID;
import gamblingauthoritiy.BetLoggingAuthority;
import gamblingauthoritiy.BetToken;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BettingRoundTest {

    /**
     * @verifies HaveUniqueBettingRoundID
     * @see BettingRound#BettingRound(BettingRoundID, BetToken)
     */
    @Test
    public void BettingRound_shouldHaveUniqueBettingRoundID() throws Exception {
        //arrange
        BettingRoundID id1 = mock(BettingRoundID.class);
        BetToken token1 = mock(BetToken.class);

        BettingRoundID id2 = mock(BettingRoundID.class);
        BetToken token2 = mock(BetToken.class);

        //act
        BettingRound bettingRound1 = new BettingRound(id1, token1);
        BettingRound bettingRound2 = new BettingRound(id2, token2);

        //assert
        assertThat(bettingRound1.getBettingRoundID()).isNotEqualTo(bettingRound2.getBettingRoundID());
    }

    /**
     * @verifies placeBetInTheSetOfBets
     * @see BettingRound#placeBet(Bet)
     */
    @Test
    public void placeBet_shouldPlaceBetInTheSetOfBets() throws Exception {
        //arrange
        //arrange DOCs
        Bet bet1 = mock(Bet.class);
        Bet bet2 = mock(Bet.class);
        Bet bet3 = mock(Bet.class);

        BettingRoundID id = mock(BettingRoundID.class);
        BetToken token = mock(BetToken.class);

        //arrange SUT
        BettingRound SUT = new BettingRound(id, token);

        //act
        SUT.placeBet(bet1);
        SUT.placeBet(bet2);
        SUT.placeBet(bet3);

        //assert
        assertTrue(SUT.getAllBetsMade().contains(bet1));
        assertTrue(SUT.getAllBetsMade().contains(bet2));
        assertTrue(SUT.getAllBetsMade().contains(bet3));
    }

    /**
     * @verifies throwIllegalArgumentExceptionIfBetIsNull
     * @see BettingRound#placeBet(casino.bet.Bet)
     */
    @Test
    public void placeBet_shouldThrowIllegalArgumentExceptionIfBetIsNull() throws Exception {
        //arrange
        //arrange DOCs
        Bet bet = null;
        BettingRoundID id = mock(BettingRoundID.class);
        BetToken token = mock(BetToken.class);

        //arrange SUT
        BettingRound SUT = new BettingRound(id, token);

        //act & assert
        assertThrows(IllegalArgumentException.class, () -> SUT.placeBet(bet), "Bet is not null, did not throw IllegalArgumentException");
    }

    /**
     * @verifies returnTheNumberOfBetsMade
     * @see BettingRound#numberOFBetsMade()
     */
    @Test
    public void numberOFBetsMade_shouldReturnTheNumberOfBetsMade() throws Exception {
        //arrange
        BettingRoundID id = mock(BettingRoundID.class);
        BetToken token = mock(BetToken.class);
        Bet bet1 = mock(Bet.class);
        Bet bet2 = mock(Bet.class);
        Bet bet3 = mock(Bet.class);

        //arrange SUT
        BettingRound SUT = new BettingRound(id, token);
        SUT.placeBet(bet1);
        SUT.placeBet(bet2);
        SUT.placeBet(bet3);

        //act
        int nrOfBets = SUT.numberOFBetsMade();

        //assert
        assertThat(nrOfBets).isEqualTo(3);

    }

    /**
     * @verifies HaveSameHashCodeWhenBettingRoundIDAndTokenAreTheSame
     * @see BettingRound#equals(Object)
     */
    @Test
    public void equals_shouldHaveSameHashCodeWhenBettingRoundIDAndTokenAreTheSame() throws Exception {
        //arrange
        BettingRoundID id1 = mock(BettingRoundID.class);
        BetToken token1 = mock(BetToken.class);

        //act
        BettingRound bettingRound1 = new BettingRound(id1, token1);
        BettingRound bettingRound2 = new BettingRound(id1, token1);

        //assert
        assertThat(bettingRound1.hashCode()).isEqualTo(bettingRound2.hashCode());
    }


    /**
     * @verifies ShouldReturnTrueWhenComparableBettingRoundsHaveSameIDsAndSameToken
     * @see BettingRound#equals(Object)
     */
    @Test
    public void equals_shouldShouldReturnTrueWhenComparableBettingRoundsHaveSameIDsAndSameToken() throws Exception {
        //arrange
        BettingRoundID id1 = mock(BettingRoundID.class);
        BetToken token1 = mock(BetToken.class);

        BettingRound bettingRound1 = new BettingRound(id1, token1);
        BettingRound bettingRound2 = new BettingRound(id1, token1);

        //act
        boolean comparsionResult = bettingRound1.equals(bettingRound2);

        //assert
        assertTrue(comparsionResult);
    }

    /**
     * @verifies ShouldReturnFalseWhenComparableBettingRoundsHaveDifferentIDsAndSameToken
     * @see BettingRound#equals(Object)
     */
    @Test
    public void equals_shouldShouldReturnFalseWhenComparableBettingRoundsHaveDifferentIDsAndSameToken() throws Exception {
        //arrange
        BettingRoundID id1 = mock(BettingRoundID.class);
        BettingRoundID id2 = mock(BettingRoundID.class);
        BetToken token1 = mock(BetToken.class);

        BettingRound bettingRound1 = new BettingRound(id1, token1);
        BettingRound bettingRound2 = new BettingRound(id2, token1);

        //act
        boolean comparsionResult = bettingRound1.equals(bettingRound2);

        //assert
        assertFalse(comparsionResult);
    }

    /**
     * @verifies ShouldReturnFalseWhenComparableBettingRoundsHaveDifferentIDsAndDifferentToken
     * @see BettingRound#equals(Object)
     */
    @Test
    public void equals_shouldShouldReturnFalseWhenComparableBettingRoundsHaveDifferentIDsAndDifferentToken() throws Exception {
        //arrange
        BettingRoundID id1 = mock(BettingRoundID.class);
        BettingRoundID id2 = mock(BettingRoundID.class);
        BetToken token1 = mock(BetToken.class);
        BetToken token2 = mock(BetToken.class);

        BettingRound bettingRound1 = new BettingRound(id1, token1);
        BettingRound bettingRound2 = new BettingRound(id2, token2);

        //act
        boolean comparsionResult = bettingRound1.equals(bettingRound2);

        //assert
        assertFalse(comparsionResult);
    }
}
