package casino.idfactory;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class IDFactoryTest {

    private static Stream<Arguments> VALID_ID_TYPES() {
        return Stream.of(
                arguments("betID"),
                arguments("cardID"),
                arguments("bettingroundID"),
                arguments("gamingMachineID")
        );
    }

    private static Stream<Arguments> INVALID_ID_TYPES() {
        return Stream.of(
                arguments("gameId"),
                arguments("casinoID"),
                arguments("cashierID"),
                arguments("INVALID ID")
        );
    }

    @ParameterizedTest
    @MethodSource("VALID_ID_TYPES")
    void generateID_ShouldReturnInstanceOfGeneralIDTypeAccordingToThePassedValidIdType(String ValidIdType){
        assertNotNull(IDFactory.generateID(ValidIdType));
    }

    @ParameterizedTest
    @MethodSource("INVALID_ID_TYPES")
    void generateID_ShouldReturnNullForPassedInvalidIdType(String InvalidIdType){
        assertNull(IDFactory.generateID(InvalidIdType));
    }


}