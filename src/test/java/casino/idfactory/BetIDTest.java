package casino.idfactory;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BetIDTest {
    /**
     * @verifies ShouldReturnTrueWhenComparableCardsHaveTheSameHashCode
     * @see BetID#equals(Object)
     */
    @Test
    void equals_ShouldReturnTrueWhenComparableCardsHaveTheSameIDsAndCreationTimes() {
        //Arrange
        BetID betID1 = new BetID();
        BetID betID2 = mock(BetID.class);

        when(betID2.GetId()).thenReturn(betID1.GetId());
        when(betID2.GetCreationTime()).thenReturn(betID1.GetCreationTime());

        //Act
        boolean comparsionResult = betID1.equals(betID2);

        //Assert
        assertTrue(comparsionResult);
    }


    /**
     * @verifies ShouldReturnFalseComparableCardsHaveDifferentHashCode
     * @see BetID#equals(Object)
     */
    @Test
    void equals_ShouldReturnFalseWhenComparableCardsHaveDifferentIDsAndSameCreationTime() {
        //Arrange
        BetID betID1 = new BetID();
        BetID betID2 = mock(BetID.class);

        when(betID2.GetId()).thenReturn(new UUID(betID1.GetId().getMostSignificantBits()+ 10L,betID1.GetId().getLeastSignificantBits()+10L));
        when(betID2.GetCreationTime()).thenReturn(betID1.GetCreationTime());
        //Act
        boolean comparsionResult = betID1.equals(betID2);

        //Assert
        assertFalse(comparsionResult);
    }

    /**
     * @verifies ShouldReturnFalseComparableCardsHaveDifferentHashCode
     * @see BetID#equals(Object)
     */
    @Test
    void equals_ShouldReturnFalseWhenComparableCardsHaveTheSameIDsAndDifferentCreationTime() {
        //Arrange
        BetID betID1 = new BetID();
        BetID betID2 = mock(BetID.class);

        when(betID2.GetId()).thenReturn(betID1.GetId());
        when(betID2.GetCreationTime()).thenReturn(Instant.ofEpochMilli(betID1.GetCreationTime().toEpochMilli() - 1000));
        //Act
        boolean comparsionResult = betID1.equals(betID2);

        //Assert
        assertFalse(comparsionResult);
    }
    /**
     * @verifies ShouldReturnFalseComparableCardsHaveDifferentHashCode
     * @see BetID#equals(Object)
     */
    @Test
    void equals_ShouldReturnFalseWhenComparableCardsHaveDifferentIDs() {
        //Arrange
        BetID betID1 = new BetID();
        BetID betID2 = new BetID();

        //Act
        boolean comparsionResult = betID1.equals(betID2);

        //Assert
        assertFalse(comparsionResult);
    }

    /**
     * @verifies ShouldReturnMinusOneWhenTheCardComparedHasBiggerIdValueThenTheReferenceCard
     * @see BetID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldReturnMinusOneWhenTheCardComparedHasBiggerIdValueThenTheReferenceCard() {
        //Arrange
        BetID betID1 = new BetID();
        BetID betID2 = mock(BetID.class);

        when(betID2.GetId()).thenReturn(new UUID(betID1.GetId().getMostSignificantBits()+ 10L,betID1.GetId().getLeastSignificantBits()+10L));

        //Act
        int comparisonResult = betID1.compareTo(betID2);

        //Assert
        assertEquals(comparisonResult,-1);
    }

    /**
     * @verifies ShouldReturnOneWhenTheCardComparedHasSmallerIdValueThenTheReferenceCard
     * @see BetID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldReturnMinusOneWhenTheCardComparedHasLesserIdValueThenTheReferenceCard() {
        //Arrange
        BetID betID1 = new BetID();
        BetID betID2 = mock(BetID.class);

        when(betID2.GetId()).thenReturn(new UUID(betID1.GetId().getMostSignificantBits()-10L,betID1.GetId().getLeastSignificantBits()-10L));

        //Act
        int comparisonResult = betID1.compareTo(betID2);

        //Assert
        assertEquals(comparisonResult,1);
    }

    /**
     * @verifies ShouldReturnZeroWhenComparableCardsHaveTheSameIDs
     * @see BetID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldReturnMinusOneWhenTheCardComparedIsCreatedAtTheSameTimeWithTheTheReferenceCardAndHasTheSameId() {
        //Arrange
        BetID betID1 = new BetID();
        BetID betID2 = mock(BetID.class);

        when(betID2.GetId()).thenReturn(betID1.GetId());
        when(betID2.GetCreationTime()).thenReturn(betID1.GetCreationTime());

        //Act
        int comparisonResult = betID1.compareTo(betID2);

        //Assert
        assertEquals(comparisonResult,0);
    }

    /**
     * @verifies ShouldReturnMinusOneWhenTheCardComparedIsCreatedBeforeTheReferenceCard
     * @see BetID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldReturnMinusOneWhenTheCardComparedIsCreatedBeforeTheReferenceCardAndHasTheSameId() {
        //Arrange
        BetID betID1 = new BetID();
        BetID betID2 = mock(BetID.class);

        when(betID2.GetId()).thenReturn(betID1.GetId());
        when(betID2.GetCreationTime()).thenReturn(Instant.ofEpochMilli(betID1.GetCreationTime().toEpochMilli() - 1000));

        //Act
        int comparisonResult = betID1.compareTo(betID2);

        //Assert
        assertEquals(comparisonResult,1);
    }

    /**
     * @verifies ShouldReturnMinusOneWhenTheCardComparedIsCreatedAfterTheReferenceCard
     * @see BetID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldReturnMinusOneWhenTheCardComparedIsCreatedAfterTheReferenceCardAndHasTheSameId() {
        BetID betID1 = new BetID();
        BetID betID2 = mock(BetID.class);

        when(betID2.GetId()).thenReturn(betID1.GetId());
        when(betID2.GetCreationTime()).thenReturn(Instant.ofEpochMilli(betID1.GetCreationTime().toEpochMilli() + 1000));

        int comparisonResult = betID1.compareTo(betID2);

        assertEquals(comparisonResult,-1);
    }

    /**
     * @verifies ShouldThrowNullPointerExceptionWhenTheComparedValueIsNull
     * @see BetID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldThrowNullPointerExceptionWhenTheComparedValueIsNull() {
        BetID betID1 = new BetID();
        assertThrows(NullPointerException.class,() -> betID1.compareTo(null));
    }
}
