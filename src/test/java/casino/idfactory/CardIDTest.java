package casino.idfactory;

import casino.gamingmachine.GamingMachine;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CardIDTest {


    /**
     * @verifies ShouldReturnTrueWhenComparableCardsHaveTheSameHashCode
     * @see CardID#equals(Object)
     */
    @Test
    void equals_ShouldReturnTrueWhenComparableCardsHaveTheSameIDsAndCreationTimes() {
        //Arrange
        CardID card1 = new CardID();
        CardID card2 = mock(CardID.class);

        when(card2.GetId()).thenReturn(card1.GetId());
        when(card2.GetCreationTime()).thenReturn(card1.GetCreationTime());

        //Act
        boolean comparsionResult = card1.equals(card2);

        //Assert
        assertTrue(comparsionResult);
    }


    /**
     * @verifies ShouldReturnFalseComparableCardsHaveDifferentHashCode
     * @see CardID#equals(Object)
     */
    @Test
    void equals_ShouldReturnFalseWhenComparableCardsHaveDifferentIDsAndSameCreationTime() {
        //Arrange
        CardID card1 = new CardID();
        CardID card2 = mock(CardID.class);

        when(card2.GetId()).thenReturn(new UUID(card1.GetId().getMostSignificantBits()+ 10L,card1.GetId().getLeastSignificantBits()+10L));
        when(card2.GetCreationTime()).thenReturn(card1.GetCreationTime());
        //Act
        boolean comparsionResult = card1.equals(card2);

        //Assert
        assertFalse(comparsionResult);
    }

    /**
     * @verifies ShouldReturnFalseComparableCardsHaveDifferentHashCode
     * @see CardID#equals(Object)
     */
    @Test
    void equals_ShouldReturnFalseWhenComparableCardsHaveTheSameIDsAndDifferentCreationTime() {
        //Arrange
        CardID card1 = new CardID();
        CardID card2 = mock(CardID.class);

        when(card2.GetId()).thenReturn(card1.GetId());
        when(card2.GetCreationTime()).thenReturn(Instant.ofEpochMilli(card1.GetCreationTime().toEpochMilli() - 1000));
        //Act
        boolean comparsionResult = card1.equals(card2);

        //Assert
        assertFalse(comparsionResult);
    }
    /**
     * @verifies ShouldReturnFalseComparableCardsHaveDifferentHashCode
     * @see CardID#equals(Object)
     */
    @Test
    void equals_ShouldReturnFalseWhenComparableCardsHaveDifferentIDs() {
        //Arrange
        CardID card1 = new CardID();
        CardID card2 = new CardID();

        //Act
        boolean comparsionResult = card1.equals(card2);

        //Assert
        assertFalse(comparsionResult);
    }

    /**
     * @verifies ShouldReturnMinusOneWhenTheCardComparedHasBiggerIdValueThenTheReferenceCard
     * @see CardID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldReturnMinusOneWhenTheCardComparedHasBiggerIdValueThenTheReferenceCard() {
        //Arrange
        CardID card1 = new CardID();
        CardID card2 = mock(CardID.class);

        when(card2.GetId()).thenReturn(new UUID(card1.GetId().getMostSignificantBits()+ 10L,card1.GetId().getLeastSignificantBits()+10L));

        //Act
        int comparisonResult = card1.compareTo(card2);

        //Assert
        assertEquals(comparisonResult,-1);
    }

    /**
     * @verifies ShouldReturnOneWhenTheCardComparedHasSmallerIdValueThenTheReferenceCard
     * @see CardID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldReturnMinusOneWhenTheCardComparedHasLesserIdValueThenTheReferenceCard() {
        //Arrange
        CardID card1 = new CardID();
        CardID card2 = mock(CardID.class);

        when(card2.GetId()).thenReturn(new UUID(card1.GetId().getMostSignificantBits()-10L,card1.GetId().getLeastSignificantBits()-10L));

        //Act
        int comparisonResult = card1.compareTo(card2);

        //Assert
        assertEquals(comparisonResult,1);
    }

    /**
     * @verifies ShouldReturnZeroWhenComparableCardsHaveTheSameIDs
     * @see CardID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldReturnMinusOneWhenTheCardComparedIsCreatedAtTheSameTimeWithTheTheReferenceCardAndHasTheSameId() {
        //Arrange
        CardID card1 = new CardID();
        CardID card2 = mock(CardID.class);

        when(card2.GetId()).thenReturn(card1.GetId());
        when(card2.GetCreationTime()).thenReturn(card1.GetCreationTime());

        //Act
        int comparisonResult = card1.compareTo(card2);

        //Assert
        assertEquals(comparisonResult,0);
    }

    /**
     * @verifies ShouldReturnMinusOneWhenTheCardComparedIsCreatedBeforeTheReferenceCard
     * @see CardID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldReturnMinusOneWhenTheCardComparedIsCreatedBeforeTheReferenceCardAndHasTheSameId() {
        //Arrange
        CardID card1 = new CardID();
        CardID card2 = mock(CardID.class);

        when(card2.GetId()).thenReturn(card1.GetId());
        when(card2.GetCreationTime()).thenReturn(Instant.ofEpochMilli(card1.GetCreationTime().toEpochMilli() - 1000));

        //Act
        int comparisonResult = card1.compareTo(card2);

        //Assert
       assertEquals(comparisonResult,1);
    }

    /**
     * @verifies ShouldReturnMinusOneWhenTheCardComparedIsCreatedAfterTheReferenceCard
     * @see CardID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldReturnMinusOneWhenTheCardComparedIsCreatedAfterTheReferenceCardAndHasTheSameId() {
        CardID card1 = new CardID();
        CardID card2 = mock(CardID.class);

        when(card2.GetId()).thenReturn(card1.GetId());
        when(card2.GetCreationTime()).thenReturn(Instant.ofEpochMilli(card1.GetCreationTime().toEpochMilli() + 1000));

       int comparisonResult = card1.compareTo(card2);

       assertEquals(comparisonResult,-1);
    }

    /**
     * @verifies ShouldThrowNullPointerExceptionWhenTheComparedValueIsNull
     * @see CardID#compareTo(GeneralID)
     */
    @Test
    void compareTo_ShouldThrowNullPointerExceptionWhenTheComparedValueIsNull() {
        CardID card1 = new CardID();
      assertThrows(NullPointerException.class,() -> card1.compareTo(null));
    }

}