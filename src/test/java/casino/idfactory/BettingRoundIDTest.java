package casino.idfactory;

import casino.game.BettingRound;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BettingRoundIDTest {


    /**
     * @verifies ShouldReturnTrueWhenComparableBettingRoundHaveTheSameHashCode
     *
     * @see BettingRoundID#equals(Object)
     */
    @Test
    public void equals_ShouldReturnTrueWhenComparableBettingRoundHaveTheSameHashCode(){

        //arrange
        BettingRoundID bettingRoundIDOne = new BettingRoundID();
        BettingRoundID bettingRoundIDTwo = mock(BettingRoundID.class);

        when(bettingRoundIDTwo.GetId()).thenReturn(bettingRoundIDOne.GetId());
        when(bettingRoundIDTwo.GetCreationTime()).thenReturn(bettingRoundIDOne.GetCreationTime());

        //act
        boolean comparingResults = bettingRoundIDOne.equals(bettingRoundIDTwo);
        //assert
        assertTrue(comparingResults);

    }


    /**
     * @verifies ShouldReturnFalseComparableBettingRoundHaveDifferentHashCode
     *
     * @see BettingRoundID#equals(Object)
     */
    @Test
    public void equals_ShouldReturnFalseWhenComparableBettingRoundHaveTheSameIDsAndDifferentCreationTime(){

        //arrange
        BettingRoundID bettingRoundIDOne = new BettingRoundID();
        BettingRoundID bettingRoundIDTwo = mock(BettingRoundID.class);

        when(bettingRoundIDTwo.GetId()).thenReturn(bettingRoundIDOne.GetId());
        when(bettingRoundIDTwo.GetCreationTime())
                .thenReturn(Instant
                        .ofEpochMilli(bettingRoundIDOne
                                .GetCreationTime()
                                .toEpochMilli() - 1000));

        //act
        boolean comparingResults = bettingRoundIDOne.equals(bettingRoundIDTwo);
        //assert
        assertFalse(comparingResults);

    }


    /**
     * @verifies ShouldReturnFalseComparableBettingRoundHaveDifferentHashCode
     *
     * @see BettingRoundID#equals(Object)
     */
    @Test
    public void equals_ShouldReturnFalseWhenComparableBettingRoundHaveDifferentIDsAndSameCreationTime(){

        //arrange
        BettingRoundID bettingRoundIDOne = new BettingRoundID();
        BettingRoundID bettingRoundIDTwo = mock(BettingRoundID.class);

        when(bettingRoundIDTwo.GetId()).thenReturn(new UUID(bettingRoundIDOne.GetId().getMostSignificantBits()+10L,bettingRoundIDOne.GetId().getMostSignificantBits()+10L));
        when(bettingRoundIDTwo.GetCreationTime()).thenReturn(bettingRoundIDOne.GetCreationTime());
        //act
        boolean comparingResults = bettingRoundIDOne.equals(bettingRoundIDTwo);
        //assert
        assertFalse(comparingResults);
    }


    /**
     * @verifies ShouldReturnFalseComparableBettingRoundHaveDifferentHashCode
     *
     * @see BettingRoundID#equals(Object)
     */
    @Test
    public void equals_ShouldReturnFalseWhenComparableBettingRoundHaveDifferentIDs(){

        //arrange
        BettingRoundID bettingRoundIDOne = new BettingRoundID();
        BettingRoundID bettingRoundIDTwo = new BettingRoundID();

        //act
        boolean comparingResults = bettingRoundIDOne.equals(bettingRoundIDTwo);
        //assert
        assertFalse(comparingResults);
    }

    /**
     * @verifies ShouldReturnFalseComparableBettingRoundHaveDifferentHashCode
     *
     * @see BettingRoundID#equals(Object)
     */
    @Test
    public void compareTo_ShouldThrowNullPointerExceptionWhenTheComparedValueIsNull(){
        //arrange
        BettingRoundID bettingRoundIDOne = new BettingRoundID();

        //act  //assert
        assertThrows(NullPointerException.class,() -> bettingRoundIDOne.compareTo(null));

    }


}