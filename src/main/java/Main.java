import casino.Casino;
import casino.bet.MoneyAmount;
import casino.cashier.IGamblerCard;
import casino.cashier.InvalidAmountException;
import casino.game.DefaultGame;
import casino.game.GameRule;
import casino.game.IGame;
import casino.gamingmachine.GamingMachine;
import casino.gamingmachine.IGamingMachine;
import casino.gamingmachine.NoPlayerCardException;

public class Main {
    public static void main(String[] args) {
        // Create a casino
        Casino casino = new Casino();

        // Create a poker game with max bets per round 2
        IGame poker = new DefaultGame(new GameRule(2),casino.getBettingAuthority().getLoggingAuthority(),casino.getBettingAuthority().getTokenAuthority());

        // Add the game to the casino
        casino.addGame("Poker",poker);

        //Create a poker machine
        IGamingMachine pokerMachine = new GamingMachine(casino.getTeller(),poker);

        //Create a gambler card
        IGamblerCard card = casino.getTeller().distributeGamblerCard();

        //Connect the card to the poker machine
        pokerMachine.connectCard(card);
        try {
            //Add amount to the card
            casino.getTeller().addAmount(card,new MoneyAmount(123L));
        } catch (InvalidAmountException e) {
            System.out.println("Invalid money amount");
        }
        try {
            //Place a bet
            boolean result = pokerMachine.placeBet(123L);
            System.out.println(result);
        } catch (NoPlayerCardException e) {
            System.out.println("No connected player card");
        }
    }
}

