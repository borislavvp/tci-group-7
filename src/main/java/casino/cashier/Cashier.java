package casino.cashier;


import casino.bet.Bet;
import casino.bet.MoneyAmount;
import casino.idfactory.CardID;
import gamblingauthoritiy.BetLoggingAuthority;
import gamblingauthoritiy.IBetLoggingAuthority;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* By Eda */
public class Cashier implements ICashier {

    List<GamblerCard> gamblerCardList;
    HashMap<CardID, MoneyAmount> moneyAmountHashMap;
    BetLoggingAuthority betLoggingAuthority;

    public Cashier(IBetLoggingAuthority loggingAuthority) {
        betLoggingAuthority = (BetLoggingAuthority) loggingAuthority;
        gamblerCardList = new ArrayList<>();
        moneyAmountHashMap = new HashMap<>();
    }

    /**
     * GamblerCard can be acquired by a player
     * @should callBetLoggingAuthorityToLogHandOutGamblingCard
     * @should addNewGamblerCardToTheGamblerCardsList
     * @return
     */
    @Override
    public IGamblerCard distributeGamblerCard() {
        GamblerCard newCard = new GamblerCard();
        boolean returned = gamblerCardList.add(newCard);
        if(returned){
            MoneyAmount initialAmount = new MoneyAmount(0L);
            moneyAmountHashMap.put(newCard.getCardID(), initialAmount);
            betLoggingAuthority.logHandOutGamblingCard(newCard.getCardID());
        }
        return newCard;
    }

    /**
     * GamblerCard can be handed in
     * @should callReturnBetIDsAndClearCardFromGamblerCard
     * @should callBetLoggingAuthorityToLogHandInGamblingCard
     * @should removeTheGamblerCardFromTheGamblerCardsList
     * @should removeTheGamblerCardFromTheHashMap
     * @param card card to be handed in
     */
    @Override
    public void returnGamblerCard(IGamblerCard card) {
        if(gamblerCardList.contains(card)){
            gamblerCardList.remove(card);
            moneyAmountHashMap.remove(card.getCardID());
            card.returnBetIDsAndClearCard();
            betLoggingAuthority.logHandInGamblingCard(card.getCardID(), card.returnBetIDs());
        }
    }

    /**
     * Checks if a bet is valid based on if the amount on the card can afford the bet
     * If yes, subtract it from the card
     * @should acceptTheBetIfTheAmountOnTheCardIsEnough
     * @should withdrawnTheBetAmountFromTheCard
     * @should throwBetNotValidExceptionIfBetAmountIsInvalid
     * @param card
     * @param betToCheck
     * @return
     * @throws BetNotExceptedException
     */
    @Override
    public boolean checkIfBetIsValid(IGamblerCard card, Bet betToCheck) throws BetNotExceptedException {
        if(moneyAmountHashMap.containsKey(card.getCardID())){
            MoneyAmount moneyOnCard = moneyAmountHashMap.get(card.getCardID());
            MoneyAmount betAmount = betToCheck.getMoneyAmount();
            if(moneyOnCard.getAmountInCents() >= betAmount.getAmountInCents()){
                long moneyLeft = moneyOnCard.getAmountInCents() - betAmount.getAmountInCents();
                MoneyAmount moneyLeftOnCard = new MoneyAmount(moneyLeft);
                moneyAmountHashMap.put(card.getCardID(), moneyLeftOnCard);
                return true;
            }
            else{
                throw new BetNotExceptedException();
            }
        }
        return false;
    }

    /**
     * Keeps an administration of adding money to a GamblerCard
     * @should updateMoneyOnTheGamblerCardByHashMap
     * @should throwInvalidAmountExceptionWhenMoneyAmountIsNegative
     * @param card card to add amount to
     * @param amount amount to add to card
     * @throws InvalidAmountException
     */
    @Override
    public void addAmount(IGamblerCard card, MoneyAmount amount) throws InvalidAmountException {
        if(moneyAmountHashMap.containsKey(card.getCardID())){
            if(amount.getAmountInCents() >= 0){
                moneyAmountHashMap.put(card.getCardID(), amount);
            }
            else{
                throw new InvalidAmountException();
            }
        }
    }

    public List<GamblerCard> getGamblerCardList() {
        return gamblerCardList;
    }

    public HashMap<CardID, MoneyAmount> getMoneyAmountHashMap() {
        return moneyAmountHashMap;
    }


}
