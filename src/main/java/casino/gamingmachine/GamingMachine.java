package casino.gamingmachine;


import casino.bet.Bet;
import casino.bet.BetResult;
import casino.bet.MoneyAmount;
import casino.cashier.BetNotExceptedException;
import casino.cashier.ICashier;
import casino.cashier.IGamblerCard;
import casino.cashier.InvalidAmountException;
import casino.game.IGame;
import casino.game.NoCurrentRoundException;
import casino.idfactory.GamingMachineID;
import casino.idfactory.IDFactory;

public class GamingMachine implements IGamingMachine {

    private final GamingMachineID _id;
    private final ICashier _cashier;
    private final IGame _game;
    private IGamblerCard _card;
    private Bet _openBet;

    public GamingMachine(ICashier _cashier, IGame _game) {
        this._cashier = _cashier;
        this._game = _game;
        this._id = (GamingMachineID) IDFactory.generateID("gamingMachineID");
    }


    @Override
    public boolean placeBet(long amountInCents) throws NoPlayerCardException {
        if(_card == null)
            throw new NoPlayerCardException("There is no gambler card connected to the machine!");

        _openBet = new Bet(_card.generateNewBetID(),new MoneyAmount(amountInCents));

        try {
            _cashier.checkIfBetIsValid(_card,_openBet);
            return _game.acceptBet(_openBet,this);
        } catch (BetNotExceptedException | NoCurrentRoundException e) {
            return  false;
        }
    }

    @Override
    public void acceptWinner(BetResult winResult) {
        if(winResult.getWinningBet().getBetID() == _openBet.getBetID()){
            try {
                _cashier.addAmount(_card,winResult.getAmountWon());
            } catch (InvalidAmountException e) {
                throw new IllegalArgumentException("Invalid amount!");
            }
        }
        _openBet = null;
    }

    @Override
    public GamingMachineID getGamingMachineID() {
        return this._id;
    }

    @Override
    public void connectCard(IGamblerCard card) {
        this._card = card;
    }

    @Override
    public void disconnectCard() throws CurrentBetMadeException {
        if(_openBet != null)
            throw  new CurrentBetMadeException("Cannot disconnect gambler card when there is an open bet made with it, please wait for the betting round to finish!");
        this._card = null;
    }

    public IGamblerCard GetConnectedCard(){
        return this._card;
    }

    public Bet GetOpenBet(){
        return this._openBet;
    }
}
