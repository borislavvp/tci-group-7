package casino.game;
import casino.bet.Bet;
import casino.bet.BetResult;
import casino.gamingmachine.IGamingMachine;
import gamblingauthoritiy.IBetLoggingAuthority;
import gamblingauthoritiy.IBetTokenAuthority;

/* By Redzhep  */
public class DefaultGame extends AbstractGame {

    public DefaultGame(IGameRule gameRule, IBetLoggingAuthority logger, IBetTokenAuthority tokeAPI) {
        super(gameRule, logger, tokeAPI);
    }

    /**
     * @should CreateAndStartNewBettingRound
     * @should StopTheCurrentBettingRoundAndCreateANewOneWhichBecomesTheNewCurrentRound
     */
    @Override
    public void startBettingRound() {
        BettingRound  bettingRound = this.GenerateRound();
        setBettingRound(bettingRound);
    }

    /**
     * @should CallGetBettingRoundAndPlaceABet
     * @should CheckIfARoundIsFinished
     * @should DetermineTheWinnerWhenARoundIsFinished
     * @should ThrowNewNoCurrentRoundExceptionWhenThereIsNoRound
     * @should LogTheAcceptedBetToLoggerAuthority
     *
     * @param bet the bet to be made on the betting round
     * @param gamingMachine gamingmachine which places bet on this game.
     * @return true when bet is accepted by the game, otherwise false.
     * @throws NoCurrentRoundException when no BettingRound is currently active
     */
    @Override
    public boolean acceptBet(Bet bet, IGamingMachine gamingMachine) throws NoCurrentRoundException {
        BettingRound round = this.getBettingRound();
        getMachines().add(gamingMachine);
        if(round == null)
            throw new NoCurrentRoundException();
        round.placeBet(bet);
        this.getLogger().logAddAcceptedBet(bet,this.getBettingRound().getBettingRoundID(),gamingMachine.getGamingMachineID());
        if(isBettingRoundFinished()){
            determineWinner();
        }
        return true;
    }

    /**
     * @should EndTheCurrentBettingRound
     * @should CalculateTheWinnerBasedOnTheGameRule
     * @should NotifyAllMachinesAboutTheBetResult
     * @should EndTheCurrentRoundAndLogItToTheBetLoggingAuthority
     */
    @Override
    public void determineWinner() {
        try {
            BetResult result = this.getGameRule()
                    .determineWinner(
                            this.getTokeAPI().getRandomInteger(this.getBettingRound().betToken),
                            this.getBettingRound().Bets);
            for (IGamingMachine machine:this.getMachines()) {
                machine.acceptWinner(result);
            }
            this.getLogger().logEndBettingRound(this.getBettingRound(),result);
        } catch (NoBetsMadeException e) {
            e.printStackTrace();
        }
        startBettingRound();
    }

    /**
     * @should DetermineIfTheRightNrOfBetsAreDoneBasedOnGameRule
     *
     * @return true if all necessary bets are made in the betting round, otherwise false
     */
    @Override
    public boolean isBettingRoundFinished() {
        return this.getGameRule().getMaxBetsPerRound() == this.getBettingRound().numberOFBetsMade();
    }
}
