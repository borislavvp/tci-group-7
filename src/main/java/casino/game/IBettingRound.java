package casino.game;

import casino.idfactory.BettingRoundID;
import gamblingauthoritiy.BetToken;
import casino.bet.Bet;

import java.util.Set;
/**
 * a bettinground is responsible for keeping a collection of unique Bets.
 */
public interface IBettingRound {
    BettingRoundID getBettingRoundID();
    //return the bettingRoundID initialized in the constructor -> getter but should be unique

    /**
     * add a bet to the current bettinground.
     *
     *
     * Note: also use the appropriate required methods from the gambling authority API
     *
     * @param bet
     * @return true if bet is made, otherwise false
     * @throws IllegalArgumentException when Bet is null
     */
    boolean placeBet(Bet bet) throws IllegalArgumentException;
    //if bet is null -> throw IllegalArgumentException
    //DefaultGame -> acceptBet Verify it is called
    //BetLoggingAuthority ->  logAddAcceptedBet

    /**
     *
     * @return set of all bets made in this betting round.
     */
    Set<Bet> getAllBetsMade();
    //return the set of bets -> no test for getter

    /**
     *
     * @return betToken from this betting round.
     */
    BetToken getBetToken();
    //BetToken.getUniqueTokenID(BettingRoundID) -> verify it is called

    /**
     *
     * @return number of bets made in the betting round
     */
    int numberOFBetsMade();
    //return setOfBets.size -> no test for getter
}
