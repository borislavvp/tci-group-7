package casino.game;
/* By Eda */
import casino.Casino;
import casino.gamingmachine.IGamingMachine;
import casino.idfactory.BettingRoundID;
import casino.idfactory.IDFactory;
import gamblingauthoritiy.BetToken;
import gamblingauthoritiy.IBetLoggingAuthority;
import gamblingauthoritiy.IBetTokenAuthority;
import java.util.HashSet;
import java.util.List;
abstract class AbstractGame implements IGame{
    // define only the constructor here
    private IGameRule gameRule;
    private BettingRound bettingRound;
    private HashSet<IGamingMachine> machines;
    private IBetLoggingAuthority logger;
    private IBetTokenAuthority tokeAPI;
    //In abstract game =>
    public AbstractGame(IGameRule gameRule, IBetLoggingAuthority logger, IBetTokenAuthority tokeAPI) {
        this.gameRule = gameRule;
        this.logger = logger;
        this.tokeAPI = tokeAPI;
        this.machines = new HashSet<>();
        this.bettingRound = GenerateRound();
    }
    public IGameRule getGameRule() {
        return gameRule;
    }
    public HashSet<IGamingMachine> getMachines() {
        return machines;
    }
    public IBetLoggingAuthority getLogger() {
        return logger;
    }
    public IBetTokenAuthority getTokeAPI() {
        return tokeAPI;
    }
    public BettingRound GenerateRound(){
        BettingRoundID roundId = (BettingRoundID) IDFactory.generateID("BettingRoundID");
        return new BettingRound(roundId,tokeAPI.getBetToken(roundId));
    }
    public BettingRound getBettingRound() {
        return bettingRound;
    }
    public void setBettingRound(BettingRound bettingRound) {
        this.bettingRound = bettingRound;
    }
}