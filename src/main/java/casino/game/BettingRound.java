package casino.game;

import casino.gamingmachine.GamingMachine;
import casino.idfactory.BettingRoundID;
import casino.idfactory.GamingMachineID;
import gamblingauthoritiy.BetToken;
import casino.bet.Bet;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
/* By Eda */


public class BettingRound implements IBettingRound {

    Set<Bet> Bets;
    BettingRoundID bettingRoundID;
    BetToken betToken;

    /**
     * @should HaveUniqueBettingRoundID
     * @param id
     * @param token
     */
    public BettingRound(BettingRoundID id, BetToken token) {
        Bets = new HashSet<>();
        bettingRoundID = id;
        betToken = token;
    }

    /**
     * @return unique bettingRoundID for this BettingRound
     */
    @Override
    public BettingRoundID getBettingRoundID() {
        return bettingRoundID;
    }

    /**
     * Add a bet to this BettingRound
     * @should throwIllegalArgumentExceptionIfBetIsNull
     * @should placeBetInTheSetOfBets
     * @param bet bet to be placed on this BettingRound
     * @return true if the bet is made
     *         else false
     * @throws IllegalArgumentException
     */
    @Override
    public boolean placeBet(Bet bet) throws IllegalArgumentException {
        if(bet != null){
            Bets.add(bet);
            return true;
        }
        else{
            throw new IllegalArgumentException();
        }
    }

    /**
     * @return All bets made on this BettingRound
     */
    @Override
    public Set<Bet> getAllBetsMade() {
        return Bets;
    }

    /**
     * @return unique bet token
     */
    @Override
    public BetToken getBetToken() {
        return betToken;
    }

    /**
     * @should returnTheNumberOfBetsMade
     * @return total number of bets made on this BettingRound
     */
    @Override
    public int numberOFBetsMade() {
        return Bets.size();
    }

    /**
     * @should HaveSameHashCodeWhenBettingRoundIDAndTokenAreTheSame
     * @should ShouldReturnTrueWhenComparableBettingRoundsHaveSameIDsAndSameToken
     * @should ShouldReturnFalseWhenComparableBettingRoundsHaveDifferentIDsAndSameToken
     * @should ShouldReturnFalseWhenComparableBettingRoundsHaveDifferentIDsAndDifferentToken
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BettingRound that = (BettingRound) o;
        return bettingRoundID.equals(that.bettingRoundID) && betToken.equals(that.betToken);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bettingRoundID, betToken);
    }
}
