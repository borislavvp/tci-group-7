package casino.game;

import casino.bet.Bet;
import casino.bet.BetResult;

import java.util.Set;

public class GameRule implements IGameRule{
    private final int MAX_BETS;

    public GameRule(int MAX_BETS) {
        this.MAX_BETS = MAX_BETS;
    }

    /**
     * Determine the winner from a Set of Bets, using a given random win value;
     *
     * @param randomWinValue
     * @param bets
     * @return Betresult, containing the result for the winning bet.
     * @throws NoBetsMadeException when no bets have been made yet.
     */
    @Override
    public BetResult determineWinner(Integer randomWinValue, Set<Bet> bets) throws NoBetsMadeException {
        int randomWinnerIndex = randomWinValue;
        if(bets.isEmpty())
            throw new NoBetsMadeException("There aren't any bets made!");
        if(randomWinValue < 0)
            randomWinnerIndex = randomWinValue * -1;

        while (randomWinnerIndex > bets.size()){
            randomWinnerIndex /= 10;
        }
        Bet winner = (Bet) bets.toArray()[randomWinnerIndex];
        return new BetResult(winner,winner.getMoneyAmount());
    }

    /**
     * return the maximum number of bets which are used in the calculation of the winner.
     *
     * @return
     */
    @Override
    public int getMaxBetsPerRound() {
        return MAX_BETS;
    }
}
