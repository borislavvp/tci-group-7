package casino.idfactory;

import java.time.Instant;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

/**
 * CREATE TESTS FOR THIS CLASS, implement necessary code when needed (after creating tests first)
 *
 */
public abstract class GeneralID  implements Comparable<GeneralID>  {
    private final UUID uniqueID;
    private final Instant timeStamp;

    public GeneralID() {
        this.uniqueID = UUID.randomUUID();
        this.timeStamp = Instant.now();
    }

    // TODO: implement necessary code. Add WHY you need it.

    // Each ID has to implement the Comparable interface and each ID is a GeneralID that's why it is implemented here
    // Getters are needed for the tests

    public UUID GetId(){
        return  this.uniqueID;
    }

    public Instant GetCreationTime(){
        return this.timeStamp;
    }

    /**
     *
     * @return a hash code value for this object.
     * @see Object#equals(Object)
     * @see System#identityHashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.GetId(), this.GetCreationTime());
    }

    /**
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     * @see #hashCode()
     * @see HashMap
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (! (obj instanceof GeneralID)) return false;
        GeneralID card = (GeneralID) obj;
        return this.GetId().equals(card.GetId()) &&
                this.GetCreationTime().equals(card.GetCreationTime());
    }


    @Override
    public int compareTo(GeneralID o) {
        // throw exceptions when necessary
        if(o == null){
            throw new NullPointerException();
        }
        // comparing logical attributes
        int returnValue = this.GetId().compareTo(o.GetId());
        if(returnValue == 0){
            returnValue = this.GetCreationTime().compareTo(o.GetCreationTime());
        }
        return returnValue;
    }
}
