package casino.idfactory;

import casino.bet.Bet;

/**
 * Factory for creation of GeneralID objects.
 * creation of the right object is done by specifying the type to create as a string
 * the specified type is case insensitive
 *
 * possible types:
 * betID
 * bettingroundID
 * cardID
 * gamingMachineID
 *
 * when the type is not present, null is returned.
 */
public class IDFactory {

    /**
     * generate the right generalID instance by specifying the type as a string
     * @param idType is name of the type in capitals.
     * @return an instance of the correct GeneralID object type, or null otherwise.
     */
    public static GeneralID generateID(String idType){
        if ("CardID".equalsIgnoreCase(idType)) return new CardID();
        else if("BetID".equalsIgnoreCase(idType)) return new BetID();
        else if("BettingRoundID".equalsIgnoreCase(idType)) return new BettingRoundID();
        else if("GamingMachineID".equalsIgnoreCase(idType)) return new GamingMachineID();
        return null;
    };

}
