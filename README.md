# TCI - Final Assignment
The final project of the TCI is a standalone application that simulates a casino using the gambling authority API. 
 
## Getting Started
These instructions will get you a copy of the project up
and running on your local machine for development and testing
purposes. See deployment for notes on how to deploy the project on a live system.
 
### Prerequisites
Install IntelliJ from: https://www.jetbrains.com/idea/download/#section=windows
Optionally install TestCherry plugin:
In IntelliJ go to “File -> Settings -> Plugins” and search for TestCherry
 
### Installing
 
In order to clone the project type in git bash 
```
git clone https://gitlab.com/borislavvp/tci-group-7.git 
```
 
In order to build the project run the following command
```
./gradlew build
```
In order to run all the tests, run the following command
```
./gradlew test
```
 
In order to generate a test coverage report, run the following command
```
./gradlew jacocoTestReport
```
You can find the generated report in the build/jacoco folder
 
End with an example of getting some data out of
the system or using it for a little demo
### And coding style tests
 
All the test are written **@should** style test units. It aims to make it easier to make TDD with a really cool and easy approach that consist in annotating interface method with desired behaviours
 
```
    /**
     *
     * @return
     * @should say hello, and nothing more than that
     */
    String sayHello();
 
```
So with the Test Cherry plugin, generate a test class for this interface like this one automatically:
 
```
    /**
     * @see Person#sayHello()
     * @verifies say hello, and nothing more that that
     */
    @Test
    public void sayHello_shouldSayHelloAndNothingMoreThatThat() throws Exception {
        //TODO auto-generated
        Assert.fail("Not yet implemented");
    }
 
```
 
 
 
 
 
## Built With
* [Gradle](https://gradle.org/) Gradle is a build system (open source) that is used to automate building, testing, deployment
 
## Authors
 
* **Eda Gyunesh**
* **Redzhep Molaahmed**
* **Borislav Pavlov**
 
*Initial work* [Eda Gyunesh](https://git.fhict.nl/I411516/tci-weekly-assignments)  
 
 
 

